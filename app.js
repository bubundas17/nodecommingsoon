var express         = require('express')
const http          = require('http');
const https         = require('https');
const redirectHttps = require('redirect-https')

var app = express()

var le = require('greenlock').create({
  server: 'https://acme-v01.api.letsencrypt.org/directory',
  configDir: 'certs/etc',
  approveDomains: (opts, certs, cb) => {
    if (certs) {
      opts.domains = ['tkc4you.com', 'www.tkc4you.com']
    } else {
      opts.email = 'bubundas17@gmail.com',
        opts.agreeTos = true;
    }
    cb(null, {
      options: opts,
      certs: certs
    });
  },
});

app.use(express.static('public'))


// app.listen(3000, ()=> {
//   console.log("Running The Server");
// })

http.createServer(le.middleware(redirectHttps())).listen(80, function() {
  console.log("Server Running On http" + 80);
})

https.createServer(le.httpsOptions, le.middleware(app)).listen(443, function() {
  console.log("Server Running On https" + 443);
})
